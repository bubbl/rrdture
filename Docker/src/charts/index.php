<?php
	//$mysqli = new mysqli("10.10.114.23", "wstation", "w46d2wmrqBy4NyaC", "temps");
	//$mysqli = new mysqli("10.10.127.210", "wstation", "w46d2wmrqBy4NyaC", "temps");
	$mysqli = new mysqli("localhost", "wstation", "w46d2wmrqBy4NyaC", "temps");

	if ($mysqli->connect_errno) {
		header("HTTP/1.0 500 Internal Server Error");
		exit();
	}

	$lastTemps = getLastTemps($mysqli);
	$lastAVGTemps = getAVGTemps($mysqli);
	$last24hourValues = last24hourValues($mysqli);

	$mysqli->close();

	/**if($_GET['format'] == json) {
		header('Content-Type: application/json');
		print json_encode($lastTemps);
		exit;
	}**/

	header('Refresh: 300; url=/charts/');

	function getLastTemps($mysqli) {
		$stmt = $mysqli->prepare("SELECT temp1, temp2, temp4, created_at FROM temps ORDER BY created_at DESC LIMIT 1");
		$stmt->execute();
		$stmt->bind_result($res['temp1'], $res['temp2'], $res['temp4'], $res['created_at']);
		$stmt->fetch();
		return $res;
	}

	function getAVGTemps($mysqli) {
		$stmt = $mysqli->prepare("SELECT AVG(temp1), AVG(temp2), AVG(temp4) FROM temps WHERE created_at >= '2015-12-20' - INTERVAL 7 DAY");
		$stmt->execute();
		$stmt->bind_result($res['temp1'], $res['temp2'], $res['temp4']);
		$stmt->fetch();
		return $res;
	}

	function last24hourValues($mysqli) {
		$stmt = $mysqli->prepare("SELECT temp1, temp2, temp4, created_at FROM temps WHERE created_at >= '2015-12-20' - INTERVAL 7 DAY");
		$stmt->execute();
		$stmt->bind_result($res['temp1'], $res['temp2'], $res['temp4'], $res['created_at']);
		$rows = array();

		$i = 0;
		while($stmt->fetch()) {
			$rows[$i] = array();
            foreach($res as $k=>$v)
                $rows[$i][$k] = $v;
            $i++;
		}
		return $rows;
	}

	function tempParts($temp, $index) {
		$parts = explode('.', number_format($temp, 1));
		return $parts[$index];
	}

?>
<html>
	<head>
		<title>Home Temperatures</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <script src="js/jquery-2.1.3.min.js"></script>
		<script src="amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/serial.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/common.js"></script>
		<script type="text/javascript">

			var lineChartData = [
			<?php foreach($last24hourValues as $row) { ?>
			{
                date: calcTime(<?php print strtotime($row['created_at']) * 1000; ?>, 2),
                temp1: <?php print number_format($row['temp1'], 2); ?>,
                temp2: <?php print number_format($row['temp2'], 2); ?>,
                temp4: <?php print number_format($row['temp4'], 2); ?>
            },
			<?php } ?>
			];

			function calcTime(unixTime, offset) {

				// create Date object for current location
				d = new Date(unixTime);

				// convert to msec
				// add local time zone offset
                offset=1;
				// get UTC time in msec
				utc = d.getTime() + (d.getTimezoneOffset() * 60000);

                // create new Date object for different city
                // using supplied offset
                nd = new Date(utc + (3600000*offset));
				return nd;

			}

			function formatLabel(value, valueString, axis){
				// let's say we dont' want minus sign next to negative numbers
				if(value < 0)
				{
					valueString = valueString.substr(1);
				}

				// and we also want a letter C to be added next to all labels (you can do it with unit, but anyway)
				valueString = valueString + "°C";
				return valueString;
			}

			AmCharts.ready(function () {
                var chart = new AmCharts.AmSerialChart();
                chart.dataProvider = lineChartData;
                chart.pathToImages = "amcharts/images/";
                chart.categoryField = "date";
				chart.balloon.bulletSize = 3;

                // sometimes we need to set margins manually
                // autoMargins should be set to false in order chart to use custom margin values
                chart.marginLeft = 0;
                chart.marginBottom = 0;
                chart.marginTop = 0;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
                categoryAxis.minPeriod = "ss"; // our data is daily, so we set minPeriod to DD
                categoryAxis.inside = true;
                categoryAxis.gridAlpha = 0;
                categoryAxis.tickLength = 0;
                categoryAxis.axisAlpha = 0;

                // value
                var valueAxis = new AmCharts.ValueAxis();
				valueAxis.dashLength = 2;
                valueAxis.axisAlpha = 0;
				//set label function which will format values
				valueAxis.labelFunction = formatLabel;

                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "temp1";
                graph.lineColor = "#FF0000";
                graph.negativeLineColor = "#efcc26";
				graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
                graph.bulletSize = 3; // bullet image should be a rectangle (width = height)
                chart.addGraph(graph);

				var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "temp2";
                graph.lineColor = "#0033CC"
                graph.negativeLineColor = "#3053F2";
                graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
				graph.bulletSize = 3; // bullet image should be a rectangle (width = height)
                chart.addGraph(graph);

				var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "temp4";
                graph.lineColor = "#006600";
                graph.negativeLineColor = "#3053F2";
                graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
				graph.bulletSize = 3; // bullet image should be a rectangle (width = height)
                chart.addGraph(graph);

				// CURSOR
				var chartCursor = new AmCharts.ChartCursor();
				chartCursor.cursorPosition = "mouse";
				chartCursor.categoryBalloonDateFormat = "JJ:NN, DD MMMM";
				chart.addChartCursor(chartCursor);

                // WRITE
                chart.write("chartdiv");
            });
		</script>
        <script src="js/jquery-2.1.3.min.js"></script>
		<script src="amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/serial.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/common.js"></script>
		<script type="text/javascript">

			var lineChartData = [
			<?php foreach($last24hourValues as $row) { ?>
			{
                date: calcTime(<?php print strtotime($row['created_at']) * 1000; ?>, 2),
                temp1: <?php print number_format($row['temp1'], 2); ?>,
                temp2: <?php print number_format($row['temp2'], 2); ?>,
                temp4: <?php print number_format($row['temp4'], 2); ?>
            },
			<?php } ?>
			];

			function calcTime(unixTime, offset) {

				// create Date object for current location
				d = new Date(unixTime);

				// convert to msec
				// add local time zone offset
                offset=1;
				// get UTC time in msec
				utc = d.getTime() + (d.getTimezoneOffset() * 60000);

                // create new Date object for different city
                // using supplied offset
                nd = new Date(utc + (3600000*offset));
				return nd;

			}

			function formatLabel(value, valueString, axis){
				// let's say we dont' want minus sign next to negative numbers
				if(value < 0)
				{
					valueString = valueString.substr(1);
				}

				// and we also want a letter C to be added next to all labels (you can do it with unit, but anyway)
				valueString = valueString + "°C";
				return valueString;
			}

			AmCharts.ready(function () {
                var chart = new AmCharts.AmSerialChart();
                chart.dataProvider = lineChartData;
                chart.pathToImages = "amcharts/images/";
                chart.categoryField = "date";
				chart.balloon.bulletSize = 3;

                // sometimes we need to set margins manually
                // autoMargins should be set to false in order chart to use custom margin values
                chart.marginLeft = 0;
                chart.marginBottom = 0;
                chart.marginTop = 0;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
                categoryAxis.minPeriod = "ss"; // our data is daily, so we set minPeriod to DD
                categoryAxis.inside = true;
                categoryAxis.gridAlpha = 0;
                categoryAxis.tickLength = 0;
                categoryAxis.axisAlpha = 0;

                // value
                var valueAxis = new AmCharts.ValueAxis();
				valueAxis.dashLength = 2;
                valueAxis.axisAlpha = 0;
				//set label function which will format values
				valueAxis.labelFunction = formatLabel;

                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "temp1";
                graph.lineColor = "#FF0000";
                graph.negativeLineColor = "#efcc26";
				graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
                graph.bulletSize = 3; // bullet image should be a rectangle (width = height)
                chart.addGraph(graph);

				var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "temp2";
                graph.lineColor = "#0033CC"
                graph.negativeLineColor = "#3053F2";
                graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
				graph.bulletSize = 3; // bullet image should be a rectangle (width = height)
                chart.addGraph(graph);

				var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "temp4";
                graph.lineColor = "#006600";
                graph.negativeLineColor = "#3053F2";
                graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
				graph.bulletSize = 3; // bullet image should be a rectangle (width = height)
                chart.addGraph(graph);

				// CURSOR
				var chartCursor = new AmCharts.ChartCursor();
				chartCursor.cursorPosition = "mouse";
				chartCursor.categoryBalloonDateFormat = "JJ:NN, DD MMMM";
				chart.addChartCursor(chartCursor);

                // WRITE
                chart.write("chartdiv");
            });
		</script>
        <script src="js/jquery-2.1.3.min.js"></script>
		<script src="amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/serial.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/common.js"></script>
		<script type="text/javascript">

			var lineChartData = [
			<?php foreach($last24hourValues as $row) { ?>
			{
                date: calcTime(<?php print strtotime($row['created_at']) * 1000; ?>, 2),
                temp1: <?php print number_format($row['temp1'], 2); ?>,
                temp2: <?php print number_format($row['temp2'], 2); ?>,
                temp4: <?php print number_format($row['temp4'], 2); ?>
            },
			<?php } ?>
			];

			function calcTime(unixTime, offset) {

				// create Date object for current location
				d = new Date(unixTime);

				// convert to msec
				// add local time zone offset
                offset=1;
				// get UTC time in msec
				utc = d.getTime() + (d.getTimezoneOffset() * 60000);

                // create new Date object for different city
                // using supplied offset
                nd = new Date(utc + (3600000*offset));
				return nd;

			}

			function formatLabel(value, valueString, axis){
				// let's say we dont' want minus sign next to negative numbers
				if(value < 0)
				{
					valueString = valueString.substr(1);
				}

				// and we also want a letter C to be added next to all labels (you can do it with unit, but anyway)
				valueString = valueString + "°C";
				return valueString;
			}

			AmCharts.ready(function () {
                var chart = new AmCharts.AmSerialChart();
                chart.dataProvider = lineChartData;
                chart.pathToImages = "amcharts/images/";
                chart.categoryField = "date";
				chart.balloon.bulletSize = 3;

                // sometimes we need to set margins manually
                // autoMargins should be set to false in order chart to use custom margin values
                chart.marginLeft = 0;
                chart.marginBottom = 0;
                chart.marginTop = 0;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
                categoryAxis.minPeriod = "ss"; // our data is daily, so we set minPeriod to DD
                categoryAxis.inside = true;
                categoryAxis.gridAlpha = 0;
                categoryAxis.tickLength = 0;
                categoryAxis.axisAlpha = 0;

                // value
                var valueAxis = new AmCharts.ValueAxis();
				valueAxis.dashLength = 2;
                valueAxis.axisAlpha = 0;
				//set label function which will format values
				valueAxis.labelFunction = formatLabel;

                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "temp1";
                graph.lineColor = "#FF0000";
                graph.negativeLineColor = "#efcc26";
				graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
                graph.bulletSize = 3; // bullet image should be a rectangle (width = height)
                chart.addGraph(graph);

				var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "temp2";
                graph.lineColor = "#0033CC"
                graph.negativeLineColor = "#3053F2";
                graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
				graph.bulletSize = 3; // bullet image should be a rectangle (width = height)
                chart.addGraph(graph);

				var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "temp4";
                graph.lineColor = "#006600";
                graph.negativeLineColor = "#3053F2";
                graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area graph
				graph.bulletSize = 3; // bullet image should be a rectangle (width = height)
                chart.addGraph(graph);

				// CURSOR
				var chartCursor = new AmCharts.ChartCursor();
				chartCursor.cursorPosition = "mouse";
				chartCursor.categoryBalloonDateFormat = "JJ:NN, DD MMMM";
				chart.addChartCursor(chartCursor);

                // WRITE
                chart.write("chartdiv");
            });
		</script>

	</head>
    <body>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p><a class="btn" href="https://weather.bartbania.com/">< Back</a></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    		<div class="content">
    		  <center>
                    <div id="chartdiv" style="width:80%; height:400px;"></div>
                  </center>
                </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p><a class="btn" href="https://weather.bartbania.com/">< Back</a></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </body>
</html>

