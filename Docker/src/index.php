<?php
	header('Refresh: 300; url=/');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Bart Bania">

    <title>Home Weather Station</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: https://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Prettify -->
    <link href="css/prettify.css" type="text/css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top" class="index" onload="prettyPrint()">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">Home Weather Station</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#portfolio">Graphs</a>
                    </li>
                    <li class="page-scroll">
                        <a href="/charts">Charts</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
        <div class="row">
                 <div class="col-lg-12">
                     <div class="intro-text">
                         <span class="name"><i class="fa fa-sun-o fa-3x fa-spin"></i></span>
                         <span class="skills">Historical data only.</span>
                     </div>
                 </div>
             </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Graphs | Archive</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 portfolio-item">
                    <a href="#weatherModal1" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <i class="fa fa-search-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/hour2.png" class="img-responsive" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item">
                    <a href="#weatherModal2" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <i class="fa fa-search-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/day2.png" class="img-responsive" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item">
                    <a href="#weatherModal3" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <i class="fa fa-search-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/week2.png" class="img-responsive" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item">
                    <a href="#weatherModal4" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <i class="fa fa-search-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/month2.png" class="img-responsive" alt="">
                    </a>
                </div>
                <div class="col-sm-4 portfolio-item">
                    <a href="#weatherModal5" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <i class="fa fa-search-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/year2.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>About</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                    <p>This project was born a long time ago, when I first got my hands on Raspberry Pi and started exploring <i>physical computing</i>. My first attempts, back in 2013, resulted in few corrupted sensors, lots of tangled cable and few lines of Python code. Now I reminded myself of the dust covered project and decided to take it further.</p>
                </div>
                <div class="col-lg-4">
                    <p>My initial idea was simply to logs into <a href="http://oss.oetiker.ch/rrdtool/doc/rrdtool.en.html" target="_blank"><i>Round Robin Database Tool (RRDTool)</i> <i class="fa fa-external-link"></i></a> and create simple graphs. While developing the web front-end for my project, I've decided to make more of it, added some extra features to graphs, polished website appearance and optimized data collection.</p>
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <a href="#weatherModal6" class="btn btn-lg btn-outline" data-toggle="modal">
                        <i class="fa fa-book"></i> Read more...
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Contact Me</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <form name="sentMessage" id="contactForm" novalidation>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Email Address</label>
                                <input type="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Message</label>
                                <textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <input type="submit" class="btn btn-success btn-lg" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-8">
                        <h3>Around the Web</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="https://blog.bartbania.com/" class="btn-social btn-outline"><i class="fa fa-fw fa-home"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/laptopBlues" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="https://github.com/bubbl" class="btn-social btn-outline"><i class="fa fa-fw fa-code-fork"></i></a>
                            </li>
                            <li>
                                <a href="http://www.layershift.com/" class="btn-social btn-outline"><i class="fa fa-fw fa-cloud"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <ul class="list-inline">
                            <li>
                                <a href="https://letsencrypt.org/" target="_blank"><img src="img/lets_encrypt_logo.png" /></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Bart Bania 2015
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="weatherModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Last 6 hours</h2>
                            <hr class="star-primary">
                            <a href="img/hour2.png"><img src="img/hour2.png" class="img-responsive img-centered" alt=""></a>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="weatherModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Last day</h2>
                            <hr class="star-primary">
                            <a href="img/day2.png"><img src="img/day2.png" class="img-responsive img-centered" alt=""></a>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="weatherModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Last week</h2>
                            <hr class="star-primary">
                            <a href="img/week2.png"><img src="img/week2.png" class="img-responsive img-centered" alt=""></a>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="weatherModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Last month</h2>
                            <hr class="star-primary">
                            <a href="img/month2.png"><img src="img/month2.png" class="img-responsive img-centered" alt=""></a>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="weatherModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Last year</h2>
                            <hr class="star-primary">
                            <a href="img/year2.png"><img src="img/year2.png" class="img-responsive img-centered" alt=""></a>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="weatherModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>About</h2>
                            <hr class="star-primary">
<p id="bckgrnd" style="text-shadow: 1px 0 1px #006; padding-top: 100px; margin-top: -100px;"><i class="fa fa-info-circle"></i><strong> Background</strong></p>
<p align="justify">This project was born a long time ago, when I first got my hands on Raspberry Pi and started exploring <i>physical computing</i>. My first attempts, back in 2013, resulted in few corrupted temperature sensors, lots of tangled cable and few lines of Python code. Now I reminded myself of the dust covered project and decided to take it further.</p>
<p align="justify">My initial idea was simply to log temperatures into <a href="http://oss.oetiker.ch/rrdtool/doc/rrdtool.en.html" target="_blank"><i>Round Robin Database Tool (RRDTool)</i> <i class="fa fa-external-link"></i></a> and create simple graphs. While developing the web front-end for my project, I've decided to make more of it, added some extra features to graphs, polished website appearance and optimized data collection.</p>
<p align="justify">Alongside, I've created a sister project based on MySQL database for data colleciton and <a href="http://www.amcharts.com/" target="_blank"><i>AmCharts</i> <i class="fa fa-external-link"></i></a> for visualizaiton. Both work well.</p>
<p id="hrdwr" style="text-shadow: 1px 0 1px #006; padding-top: 100px; margin-top: -100px;"><i class="fa fa-wrench"></i><strong> Hardware</strong></p>
<p align="justify">One of the easiest sensors to connect to a Raspberry Pi is the <a href="http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf" target="_blank"><i>DS18B20 digital thermometer</i> <i class="fa fa-external-link"></i></a>. It's optput is read through a multi-device 1-wire bus that is directly supported by a driver in the Linux kernel. Several sensors can be connected in parallel to the same data-wire and read individually over the bus interface by their hard-coded IDs. An extensive tutorial on connecting the sensor to Raspberry Pi was written by <a href="http://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/temperature/" target="_blank"><i>Matthew Kirk</i> <i class="fa fa-external-link"></i></a>.</p>
<p align="justify">For my proect I've used 3 sensors on ~20m of cable. While lots of tutorials advice of using 4k7&#8486; resistor (only one!), because of the length of my network (or "network weight"), I've ended up using 2k&#8486; resistor for better readings.</p>
<p align="justify">I've soldered pin rows onto <a href="http://shop.ciseco.co.uk/k001-humble-pi/" target="_blank"><i>Humble Pi</i> <i class="fa fa-external-link"></i></a> prototyping board for convenience of (re)attaching sensors.</p>
<p id="rddt" style="text-shadow: 1px 0 1px #006; padding-top: 100px; margin-top: -100px;"><i class="fa fa-pencil"></i><strong> Reading the data</strong></p>
<p align="justify">The 1-Wire drivers are not loaded by default - they need to be activated manually. To load them permanently, edit <u>/etc/modules</u> on Raspberry Pi</p>
<pre align="justify" class="prettyprint doxy">
raspberry@pi ~$ sudo vim /etc/modules

# /etc/modules: kernel modules to load at boot time.
#
# This file contains the names of kernel modules that should be loaded
# at boot time, one per line. Lines beginning with "#" are ignored.
# Parameters can be specified after the module name.

# 1-Wire devices
w1-gpio
# 1-Wire thermometer devices
w1-therm
</pre>
<p align="justify">and reboot the device.<br/>
Once connected, to check whether a sensor is detected type:</p>
<pre class="prettyprint doxy">
raspberry@pi ~$ cat /sys/bus/w1/devices/w1_bus_master1/w1_master_slaves
28-000005502198
28-00000550283e
28-000005502a61
</pre>
<p align="justify">The output is a list of sensor addresses on the 1-Wire network.<br/>
A quick check if the sensor operates as it should:</p>
<pre class="prettyprint doxy">
raspberry@pi ~$ cat /sys/bus/w1/devices/28-*/w1_slave
91 01 4b 46 7f ff 0f 10 25 : crc=25 YES
91 01 4b 46 7f ff 0f 10 25 t=25062
5b 01 4b 46 7f ff 05 10 b5 : crc=b5 YES
5b 01 4b 46 7f ff 05 10 b5 t=21687
8c 01 4b 46 7f ff 04 10 2e : crc=2e YES
8c 01 4b 46 7f ff 04 10 2e t=24750
</pre>
<p align="justify">The temperature is the last readout after 't=' in mili-degrees Celsius.</p>
<p align="justify">To make the temperature outputs more human-readable, I've decided to use Python:</p>
<pre class="prettyprint doxy">
def read_temperature(file):
  tfile = open(file)
  text = tfile.read()
  tfile.close()
  lines = text.split("\n")
  if lines[0].find("YES") > 0:
    temp = float((lines[1].split(" ")[9])[2:])
    temp /= 1000
    return temp
</pre>
Since I don't need Fahrenheit readouts, I didn't include the conversion in my code. However, if you need the temperature in F instead of Celsius (or both), there are numerous tutorials on the wide web on this subject.</p>
<p id="rrdtool" style="text-shadow: 1px 0 1px #006; padding-top: 100px; margin-top: -100px;"><i class="fa fa-database"></i><strong> RRDTool</strong></p>
<p align="justify">Now that I could read the temperature from the sensors I needed a way to store the information. Round Robin Database is perfect for this task. It's a circular database that lets you store a predefined amount of data. After initial creation it is as big as it will ever get and just contains "unknown" data.
The 'rrdtool create' command is used to setup the database:</p>
<pre class="prettyprint doxy">
rrdtool create temperature.rrd \
    --start now --step 60 \
    DS:a:GAUGE:120:-50:50 \
    DS:b:GAUGE:120:-50:50 \
    DS:c:GAUGE:120:-50:50 \
    RRA:AVERAGE:0.5:1:12 \
    RRA:AVERAGE:0.5:1:288 \
    RRA:AVERAGE:0.5:12:2016 \
    RRA:AVERAGE:0.5:288:8640 \
    RRA:AVERAGE:0.5:12:105120
</pre>
<p align="justify">This creates a database with a base data interval of 1m (--step 60), with a data range of -50 to +50 (degrees C), and some calculated averages for 6hours, day, week, month and year.<br/>
To get more of RRDTool, read an <a href="http://oss.oetiker.ch/rrdtool/tut/rrdtutorial.en.html" target="_blank"><i>extensive tutorial</i> <i class="fa fa-external-link"></i></a>.</p> 
<p align="justify">Now, to pass sensor readings to the database, I used Python again:</p>
<pre class="prettyprint doxy">
#!/usr/bin/python

import rrdtool

databaseFile = "/path/to/database/file.rrd"
MIN_TEMP = -50
ERROR_TEMP = -999.99

rrds_to_filename = {
  "a" : "/sys/bus/w1/devices/28-000005502198/w1_slave",
  "b" : "/sys/bus/w1/devices/28-00000550283e/w1_slave",
  "c" : "/sys/bus/w1/devices/28-000005502a61/w1_slave",
}

def read_all():
  template = ""
  update = "N:"
  for rrd in rrds_to_filename:
    template += "%s:" % rrd
    temp = read_temperature(rrds_to_filename[rrd])
    update += "%f:" % temp
  update = update[:-1]
  template = template[:-1]
  rrdtool.update(databaseFile, "--template", template, update)
</pre>
<p align="justify"><a href="python.txt">Full Python code <i class="fa fa-file-code-o" style="color:red;"></i></a>.</p>
<p id="grphs" style="text-shadow: 1px 0 1px #006; padding-top: 100px; margin-top: -100px;"><i class="fa fa-image"></i><strong> Creating Graphs</strong></p>
<p align="justify">Since rrdtool can also graph, a shell script running once every 10 minutes create a graph image:</p> 
<pre class="prettyprint doxy">
#!/bin/bash

RRDPATH="/path/to/database"
IMGPATH="/path/to/img/dir"
RAWCOLOUR="#FF0000"
RAWCOLOUR2="#CC3366"
RAWCOLOUR3="#336699"
RAWCOLOUR4="#006600"
RAWCOLOUR5="#000000"
TRENDCOLOUR="#FFFF00"
RRDFILE="database_file.rrd"

#hour
rrdtool graph $IMGPATH/hour.png --start -6h \
--full-size-mode \
--width=700 --height=400 \
--slope-mode \
--color=SHADEB#9999CC \
--watermark="Bart Bania" \
DEF:temp1=$RRDPATH/$RRDFILE:a:AVERAGE \
DEF:temp2=$RRDPATH/$RRDFILE:b:AVERAGE \
DEF:temp3=$RRDPATH/$RRDFILE:c:AVERAGE \
LINE2:temp1$RAWCOLOUR:"Water" \
LINE2:temp2$RAWCOLOUR4:"Main Room" \
LINE2:temp3$RAWCOLOUR5:"Outside\l" \
HRULE:0#0000FF:"freezing"
...
</pre>
<p align="justify">At the top of the script I've got pre-defined changable variables, such as paths and color codes for my convenience. This particular example creates a graph for the last 6 hours for all five sensors.</p>
<p id="sun" style="text-shadow: 1px 0 1px #006; padding-top: 100px; margin-top: -100px;"><i class="fa fa-moon-o"></i><strong> Day and night time on the graph</strong></p>
<p align="justify">Apart from a plain graph, I wanted to add a bit of flavour to it by marking day and night times, as well as dusk/dawn based on <a href="http://www.sunrisesunset.com/definitions.html" terget="_blank">Civil twilight<i class="fa fa-external-link"></i></a> calculations. Instead of writing my own code, I've decided to make it simpler and use an external tool. A great small C program for calculating sunrise and sunset came in hand.<br/><a href="https://www.risacher.org/sunwait/"
    target="_blank"><i>sunwait</i> <i class="fa fa-external-link"></i></a> gives a whole bunch of information, including civil, nautical, and astronomical twilights, so I had to extract the data that was of interest to me:</p>
<pre class="prettyprint doxy">
# Geographical position
LAT=""
LON=""

# Get the relevant data in an ugly manner
# Calculate twilight
DUSKHR=`/usr/bin/sunwait sun up $LAT $LON -p | \
sed -n '/Sun rises/{:a;n;/Nautical twilight/b;p;ba}' | \
cut -c 45-46`
DUSKMIN=`/usr/bin/sunwait sun up $LAT $LON -p | \
sed -n '/Sun rises/{:a;n;/Nautical twilight/b;p;ba}' | \
cut -c 47-48`
DAWNHR=`/usr/bin/sunwait sun up $LAT $LON -p | \
sed -n '/Sun rises/{:a;n;/Nautical twilight/b;p;ba}' | \
cut -c 30-31`
DAWNMIN=`/usr/bin/sunwait sun up $LAT $LON -p | \
sed -n '/Sun rises/{:a;n;/Nautical twilight/b;p;ba}' | \
cut -c 32-33`

# Calculate night time
SUNRISEHR=`/usr/bin/sunwait sun up $LAT $LON -p | \
sed -n '/Sun transits/{:a;n;/Civil twilight/b;p;ba}' | \
cut -c 30-31`
SUNRISEMIN=`/usr/bin/sunwait sun up $LAT $LON -p | \
sed -n '/Sun transits/{:a;n;/Civil twilight/b;p;ba}' | \
cut -c 32-33`
SUNSETHR=`/usr/bin/sunwait sun up $LAT $LON -p | \
sed -n '/Sun transits/{:a;n;/Civil twilight/b;p;ba}' | \
cut -c 45-46`
SUNSETMIN=`/usr/bin/sunwait sun up $LAT $LON -p | \
sed -n '/Sun transits/{:a;n;/Civil twilight/b;p;ba}' | \
cut -c 47-48`

# Convert hours/minutes to seconds
SUNR=$(($SUNRISEHR * 3600 + $SUNRISEMIN * 60))
SUNS=$(($SUNSETHR * 3600 + $SUNSETMIN * 60))
DUSK=$(($DUSKHR * 3600 + $DUSKMIN * 60))
DAWN=$(($DAWNHR * 3600 + $DAWNMIN * 60))
</pre>
<p align="justify">and add it to graphs as shaded area representing night times where I live.</p>
</pre>
<p align="justify"><a href="bash.txt">Full BASH code <i class="fa fa-file-code-o" style="color:red;"></i></a>.</p>
<p id="shed" style="text-shadow: 1px 0 1px #006; padding-top: 100px; margin-top: -100px;"><i class="fa fa-clock-o"></i><strong> Scheduling</strong></p>
<p align="justify">Using crontab, I can schedule automatic data collection and graph creation. Since the graphs are created mainly for this website, I've decided to use my web user's crontab:</p>
<pre class="prettyprint doxy">
root@raspberrypi ~# crontab -u www-data -e

* * * * * /path/to/python/poller.py > /dev/null 2>&1
 */10 * * * * /path/to/bash/script.sh > /dev/null 2>&1
</pre>
<p align="justify">The above invoke the tasks:
<ul>
<li>Python code addind data to RRD - every minute</li>
<li>Bash script creating graphs - every 10 minutes</li>
</ul>
</p>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>

    <!-- Prettify -->
    <script src="js/prettify.js"></script>
</body>

</html>
